extern crate diesel;
extern crate dotenv;

use diesel::prelude::*;
use self::dotenv::dotenv;
use std::env;

pub fn establish_connection() -> MysqlConnection {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    MysqlConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

// TODO: put player related helpers in a module
use schema::player::dsl::*;
use models;

pub fn create_player(
    connection: &MysqlConnection
) -> Result<String, String> {
    let insert_result = diesel::insert_into(player)
        .values((name.eq("New player")))
        .execute(connection);

    match insert_result {
        Ok(_) => Ok("success".to_string()),
        Err(_) => Err("Could not create player".to_string()),
    }
}

// To remove when module for each model, this is because of ambiguous references
use schema;

pub fn delete_all_players(connection: &MysqlConnection) -> Result<usize, String> {
    let insert_result =
        diesel::delete(player).execute(connection);

    match insert_result {
        Ok(count) => Ok(count),
        Err(e) => {
            println!("error: {:#?}", e);
            Err("Could not delete player".to_string())
        }
    }
}

pub fn get_number_of_players(connection: &MysqlConnection) -> i64 {
    if let Ok(count_results) = player.select(diesel::dsl::count_star()).first(connection) {
        return count_results;
    }
    // shouldn't happen, but whatever, match clauses are a pain in the ass.
    0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_and_delete_player() {
        let connection = establish_connection();
        // TODO: get number of players
        let number_of_players = get_number_of_players(&connection);
        if let Ok(_) = create_player(&connection) {
            assert_eq!(number_of_players + 1, get_number_of_players(&connection));
            assert!(delete_all_players(&connection).is_ok());
            assert_eq!(number_of_players, get_number_of_players(&connection));
        } else {
            // TODO: fail
        }
    }
}
