use schema::*;

#[derive(Queryable, Debug)]
pub struct Player {
    /*`id` int(6) unsigned NOT NULL AUTO_INCREMENT,*/
    pub id: i32,
    pub name: String
}