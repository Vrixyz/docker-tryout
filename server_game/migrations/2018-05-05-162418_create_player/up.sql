-- Your SQL goes here

CREATE TABLE IF NOT EXISTS `player` (
  `id` SERIAL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;